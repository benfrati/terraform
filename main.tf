terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.5.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "my_terra_vpc"
  }
}

resource "aws_subnet" "terra_subnet1" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "terra-subnet-vpc"
  }
}

data "aws_vpc" "default_vpc" {
  default = true
}

resource "aws_subnet" "terra_subnet1_default" {
  vpc_id            = data.aws_vpc.default_vpc.id
  cidr_block        = "172.31.96.0/20"
  availability_zone = "us-east-1a"
  tags = {
    Name = "terra-subnet-vpc-default"
  }
}